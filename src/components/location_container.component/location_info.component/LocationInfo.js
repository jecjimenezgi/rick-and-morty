function LocationInfo({ locationName, dimensionName, locationType }) {
  return (
    <div>
      <ul>
        <li><h2>Location: {locationName}</h2></li>
        <li><h2>Dimension: {dimensionName}</h2></li>
        <li><h2>Type: {locationType}</h2></li>
      </ul>
    </div>
  );
}

export default LocationInfo;
