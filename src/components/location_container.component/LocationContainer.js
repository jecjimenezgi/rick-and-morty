import {useEffect,useState} from "react"
import LocationInfo from "./location_info.component/LocationInfo"

function LocationContainer({location,setResidents}){

    const [viewLocation, setViewLocation] = useState(null)
    const [locationName, setLocationName] = useState("")
    const [dimensionName, setDimensionName] = useState("")
    const [locationType, setLocationType] = useState("")

    useEffect(()=> { 
        if (location){
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            
            fetch("https://rickandmortyapi.com/api/location/" + location, requestOptions)
                .then(response => response.json())
                .then(result => {
                    setLocationName(result.name)
                    setDimensionName(result.dimension)
                    setLocationType(result.type)
                    setResidents(result.residents.splice(0,10))
                })
                .catch(error => console.log('error', error));

            
        }

    },[location,setResidents])

    useEffect(()=> { 
        if(locationName && dimensionName  && locationType){
            setViewLocation(
                <LocationInfo locationName={locationName} dimensionName={dimensionName} locationType={locationType} ></LocationInfo>
            )
        }
    }, [locationName,dimensionName,locationType])

    return (<div>
        {viewLocation}
    </div>)
}

export default LocationContainer