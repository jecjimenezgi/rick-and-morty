import "./SelecBox.css"
import list_places from "../../settings/places.json";
import { useEffect, useState } from "react";


function SelectBox({ setLocation, setResidents }) {
  const [values] = useState(list_places);
  const [form, setForm] = useState(null);


  useEffect(() => {
    if (values) {
    function handleSubmit(e) {
        e.preventDefault();
        setLocation(e.target.getElementsByTagName("select")[0].value);
        }
        
      let select = (
        <div>
          <form onSubmit={handleSubmit}>
            <select name="seletc-places" id="selec-places">
              {values.map((i) => {
                return (
                  <option key={i.id} value={i.id}>
                    {i.name}
                  </option>
                );
              })}
            </select>
            <button type="submit">Search</button>
          </form>
        </div>
      );
      setForm(select);
    }
  }, [values,setLocation]);

  return <div>{form}</div>;
}

export default SelectBox;
