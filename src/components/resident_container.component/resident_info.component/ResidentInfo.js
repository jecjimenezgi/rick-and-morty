import "./ResidentInfo.css"

function ResidentInfo({name, img, status, origin, episode}){

    return ( 
        <div className="card">
            <ul>
                <li><h3>{name}</h3></li>
                <li><img src={img} alt={name}></img></li>
                <li><h3>status: {status}</h3></li>
                <li><h3>orgin: {origin}</h3></li>
                <li><h3># episodes: {episode}</h3></li>
            </ul>
        </div>
    )

}

export default ResidentInfo