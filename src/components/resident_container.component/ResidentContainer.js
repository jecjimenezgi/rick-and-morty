import ResidentInfo from "./resident_info.component/ResidentInfo.js";
import { useEffect, useState } from "react";

function ResidentContainer({ resident }) {
  const [viewResident, setViewResident] = useState(null);
  const [name, setName] = useState("");
  const [img, setImg] = useState("");
  const [status, setStatus] = useState("");
  const [origin, setOrigin] = useState("");
  const [episode, setEpisode] = useState(0);

  useEffect(() => {
    if (resident) {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      fetch(resident, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          setName(result.name);
          setImg(result.image);
          setOrigin(result.origin.name);
          setStatus(result.status);
          setEpisode(result.episode.length);
        })
        .catch((error) => console.log("error", error));
    }
  }, [resident]);

  useEffect(() => {

    if (name && img && status && origin && episode) {

      setViewResident(
        <ResidentInfo
          name={name}
          img={img}
          status={status}
          origin={origin}
          episode={episode}
        ></ResidentInfo>
      );
    }
  }, [name, img, status, origin, episode]);

  return <div className="col-3">{viewResident}</div>;
}

export default ResidentContainer;
