import './App.css';
import SelectBox from './components/select_box.component/SelectBox.js';
import LocationContainer from './components/location_container.component/LocationContainer.js';
import ResidentContainer from './components/resident_container.component/ResidentContainer.js';
import { useEffect , useState } from "react";

function App() {
  const [location, setLocation] = useState(Math.floor(Math.random() * (50)))
  const [residents, setResidents] = useState([])
  const [viewLocation, setViewLocation] = useState(null)
  const [viewResidents, setViewResidents] = useState(null)

  useEffect( () => { 
    if (location){ 
      setViewLocation( <LocationContainer location={location} setResidents={setResidents}></LocationContainer>)
    }
  }, [location])

  useEffect(() => { 
    if (residents){
      setViewResidents(
        residents.map( resident => { 
          return <ResidentContainer key={resident} resident={resident}></ResidentContainer>
        })
      )
    }

  },[residents])

  return (
    <div className="App">
      <SelectBox setLocation={setLocation}></SelectBox>
      {viewLocation}
      <div className="row">
        {viewResidents}
      </div>
    </div>
  );
}

export default App;
